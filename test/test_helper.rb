ENV['RAILS_ENV'] ||= 'test'
require 'simplecov'
SimpleCov.start
require File.expand_path('../config/environment', __dir__)
require 'rails/test_help'
require 'webmock/minitest'

module ActiveSupport
  class TestCase
    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    setup do
      stub_request(:get, 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?sCepOrigem=01321-001&sCepDestino=70040-020&nVlPeso=1&nVlComprimento=16&nVlLargura=11&nVlAltura=2&nVlDiametro=0,0&nCdFormato=1&sCdMaoPropria=N&sCdAvisoRecebimento=N&nVlValorDeclarado=0,00&nCdServico=04014,04510&nCdEmpresa=&sDsSenha=&StrRetorno=xml')
        .with(headers: {
                'Accept' => '*/*',
                'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'User-Agent' => 'Ruby'
              })
        .to_return(status: 404, body: file_fixture('correios-frete.xml').read, headers: {
                     'content-type' => 'text/xml;charset=iso-8859-1'
                   })
    end
  end
end
