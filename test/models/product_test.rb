require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test 'should be invalid' do
    product = Product.new

    assert_not product.valid?
  end

  test 'should be valid' do
    product = new_valid_product

    assert product.valid?
    assert product.save
  end

  test 'should be invalid without name' do
    product = new_valid_product(name: nil)

    assert_not product.valid?
  end

  test 'should be invalid if name is less than 3 characters' do
    product = new_valid_product(name: 'aa')

    assert_not product.valid?
    assert_not product.save
  end

  test 'should be invalid without description' do
    product = new_valid_product(description: nil)

    assert_not product.valid?
    assert_not_nil product.errors[:description]
  end

  test 'should be invalid if description is less than 20 characters' do
    product = new_valid_product(description: 'bad_desc')

    assert_not product.valid?
    assert_not product.save
  end

  test 'should be invalid without value' do
    product = new_valid_product(value: nil)

    assert_not product.valid?
    assert_not_nil product.errors[:value]
  end

  test 'should be invalid with negative value' do
    product = new_valid_product(value: -2)

    assert_not product.valid?
  end

  test 'should be invalid if value is not a number' do
    product1 = new_valid_product(value: 'string')
    product2 = new_valid_product(value: true)

    assert_not product1.valid?
    assert_not product2.valid?
  end

  test 'should be invalid without height' do
    product = new_valid_product(height: nil)

    assert_not product.valid?
    assert_not_nil product.errors[:height]
  end

  test 'should be invalid with height less than 2' do
    product = new_valid_product(height: 1)

    assert_not product.valid?
  end

  test 'should be valid with height equals 2' do
    product = new_valid_product(height: 2)

    assert product.valid?
    assert product.save
  end

  test 'should be invalid if height is not a number' do
    product1 = new_valid_product(height: 'string')
    product2 = new_valid_product(height: true)

    assert_not product1.valid?
    assert_not product2.valid?
  end

  test 'should be invalid without width' do
    product = new_valid_product(width: nil)

    assert_not product.valid?
    assert_not_nil product.errors[:width]
  end

  test 'should be invalid with negative width' do
    product = new_valid_product(width: -2)

    assert_not product.valid?
  end

  test 'should be invalid if width is not a number' do
    product1 = new_valid_product(width: 'string')
    product2 = new_valid_product(width: true)

    assert_not product1.valid?
    assert_not product2.valid?
  end

  test 'should be invalid without weight' do
    product = new_valid_product(weight: nil)

    assert_not product.valid?
    assert_not_nil product.errors[:weight]
  end

  test 'should be invalid with negative weight' do
    product = new_valid_product(weight: -2)

    assert_not product.valid?
  end

  test 'should be invalid if weight is not a number' do
    product1 = new_valid_product(weight: 'string')
    product2 = new_valid_product(weight: true)

    assert_not product1.valid?
    assert_not product2.valid?
  end

  test 'should be invalid without length' do
    product = new_valid_product(length: nil)

    assert_not product.valid?
    assert_not_nil product.errors[:length]
  end

  test 'should be invalid with negative length' do
    product = new_valid_product(length: -2)

    assert_not product.valid?
  end

  test 'should be invalid if length is not a number' do
    product1 = new_valid_product(length: 'string')
    product2 = new_valid_product(length: true)

    assert_not product1.valid?
    assert_not product2.valid?
  end

  test 'should be set min value of 2 for height in the shipping calculation' do
    product1 = new_valid_product(height: 1)
    product1.validate_minimal_shipping_params!

    assert_equal 2, product1.height
  end

  test 'should be set min value of 11 for width in the shipping calculation' do
    product1 = new_valid_product(width: 11)
    product1.validate_minimal_shipping_params!

    assert_equal 11, product1.width
  end

  test 'should be set min value of 16 for length in the shipping calculation' do
    product1 = new_valid_product(length: 16)
    product1.validate_minimal_shipping_params!

    assert_equal 16, product1.length
  end

  private

  def new_valid_product(options = {})
    Product.new({
      name: 'Product 1',
      description: 'Product 1 Description',
      value: 10,
      height: 2,
      width: 11,
      weight: 0,
      length: 16
    }.merge(options))
  end
end
