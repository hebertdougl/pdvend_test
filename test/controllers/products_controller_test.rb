require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test 'should get index' do
    get products_url, as: :json
    assert_response :success
  end

  test 'should create product' do
    assert_difference('Product.count') do
      post products_url, params: {
        product: {
          description: @product.description,
          height: @product.height,
          length: @product.length,
          name: @product.name,
          value: @product.value,
          weight: @product.weight,
          width: @product.width
        }
      }, as: :json
    end

    assert_response 201
  end

  test 'should not create product' do
    post products_url, params: {
      product: {
        description: @product.description,
        height: @product.height,
        length: @product.length,
        name: @product.name,
        value: nil,
        weight: @product.weight,
        width: @product.width
      }
    }, as: :json

    assert_response 422
  end

  test 'should show product' do
    get product_url(@product), as: :json
    assert_response :success
  end

  test 'should show that the product was not found' do
    assert_raises(ActiveRecord::RecordNotFound) do
      get product_url(@product.id + 1), as: :json
    end
  end

  test 'should show freight data product' do
    get product_url(@product), as: :json

    json = JSON.parse(@response.body)

    assert_equal @product.id, json['id']
    assert_not_nil json['frete']
  end

  test 'should update product' do
    patch product_url(@product), params: {
      product: {
        description: @product.description,
        height: @product.height,
        length: @product.length,
        name: @product.name,
        value: @product.value,
        weight: @product.weight,
        width: @product.width
      }
    }, as: :json
    assert_response 200
  end

  test 'should not update product' do
    patch product_url(@product), params: {
      product: {
        description: @product.description,
        height: @product.height,
        length: @product.length,
        name: @product.name,
        value: -10,
        weight: @product.weight,
        width: @product.width
      }
    }, as: :json
    assert_response 422
  end

  test 'should destroy product' do
    assert_difference('Product.count', -1) do
      delete product_url(@product), as: :json
    end

    assert_response 204
  end
end
