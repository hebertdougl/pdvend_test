class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.decimal :value, precision: 8, scale: 2
      t.integer :height
      t.integer :width
      t.integer :weight
      t.integer :length

      t.timestamps
    end
  end
end
