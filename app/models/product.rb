
class Product < ApplicationRecord
  validates :name, presence: true, length: { minimum: 3 }
  validates :description, presence: true, length: { minimum: 20 }
  validates :value, presence: true, numericality: { greater_than: 0 }
  validates :height, presence: true, numericality: { greater_than_or_equal_to: 2 }
  validates :width, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :weight, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :length, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def frete
    validate_minimal_shipping_params!
    frete = Correios::Frete::Calculador.new(
      cep_origem: '01321-001',
      cep_destino: '70040-020',
      peso: weight,
      comprimento: length,
      largura: width,
      altura: height
    )

    frete.calcular(:sedex, :pac)
  end

  def validate_minimal_shipping_params!
    self.height = 2 if height < 2
    self.width = 11 if width < 11
    self.length = 16 if length < 16

    self
  end
end
