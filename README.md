# Product API

Project created for a selective process and has as scope the development of API REST for the creation and visualization of a product. And as a requirement during the visualization is made the calculation of freight provided from an external api.

## Prerequisites

* Ruby 2.3+
* Rails 5.0+
* Postgresql 9.4+

## Installation

### Clone the project

```console
    $ git clone https://gitlab.com/hebertdougl/pdvend_test.git
    $ cd pdvend_test
```

### Install an Configure RVM

Install RVM with curl in command line

```console
    $ \curl -L https://get.rvm.io | bash -s stable --rails
```

Create your rvm gemset

```console
    $ rvm gemset use ruby_version@gemset_name --create
```

### Install dependencies

```console
    $ bundle install
```

### Configure Database

Create user or role

```console
  $ sudo -u postgres psql
```

```console
  create role pdvend with createdb login password 'password1';
  \q
```

Setup database:

```ruby
  $ rake db:setup
```

Run migrations

```ruby
  $ rake db:migrate
```
## Api Endpoints

### Create Product

* Routes

```
POST   /products
```

* Body request example

```
{
	"product": {
		"name": "Product 2",
		"description": "Desription 2",
		"value": 10,
		"height": 2,
		"width": 15,
		"weight": 0.3,
		"length": 30
	}
}
```

### Show Product

* Routes

```
GET   /products/:id
```

## Tools

### Run the test suite

```ruby
    $ rake test
```

### Run the ruby static code analyzer

This project uses rubocop gem to ruby static code analyzer, to run the analysis

```console
    $ rubocop
```

### Run code coverage

This project uses SimpleCov gem to code coverage analysis, run the coverage

```console
   $ rake test
```

And view the coverage/index.html file to verify the code coverage

### Continuos Integration

To make continuous integration of the project, Gitlab CI was used to execute two jobs

* Run and ensure that all suite tests pass
* Run rubocop to ensure static code analysis





